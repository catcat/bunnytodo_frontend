import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

export const MainList = props => {

    return (
        props.users.map((item, i) => (
            <tr>
                <th>{item.id}</th>
                <td>{item.name}</td>
                <td>
                    <Button color="info" onClick={() => props.showModalUser(i)}>Update</Button>
                    <Button color="secondary" onClick={() => props.showModalDelete(i)}>Delete</Button>
                    <Button color="primary" onClick={() => props.showModal(i)} >{item.tasks.length ? 'View Tasks' : 'Add Task'}</Button>
                </td>
            </tr>
        ))
    );
};