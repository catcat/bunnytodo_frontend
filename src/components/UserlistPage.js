import React, { useState, useEffect } from "react";
import UserService from "../services/userservice";
import { Button, Row, Col, Table, Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input } from "reactstrap";
import { MainList } from "./MainList";
import './userlist.css'

const TASK_STATUS = {
    1: 'To Do',
    2: 'Done',
    TODO: 1,
    DONE: 2
}

export const UserlistPage = props => {
    const [modalTask, setModalTask] = useState(false);
    const [modalUpdate, setModalUpdate] = useState(false);
    const [modalDelete, setModalDelete] = useState(false);
    const [modalCreate, setModalCreate] = useState(false);
    const [userSelected, setUserSelected] = useState(null);
    const [taskSelected, setTaskSelected] = useState(null);
    const [users, setUsers] = useState([]);
    const [editingMode, setEditingMode] = useState(false);

    const initialInputState = { name: '' };
    const [eachEntry, setEachEntry] = useState(initialInputState);
    const initialInputStateTask = { description: '' };
    const [eachEntryTask, setEachEntryTask] = useState(initialInputStateTask);
    const [eachEntryUser, setEachEntryUser] = useState(initialInputState);

    const toggleTask = () => setModalTask(!modalTask);
    const toggleUpdate = () => setModalUpdate(!modalUpdate);
    const toggleDelete = () => setModalDelete(!modalDelete);
    const toggleCreate = () => setModalCreate(!modalCreate);

    useEffect(() => {
        retrieveUsers();
    }, []);

    const retrieveUsers = () => {
        UserService.getAllUsers()
            .then(response => {
                setUsers(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    const handleModal = index => {
        setUserSelected(users[index])
        toggleTask()
    }

    const handleUpdateUser = index => {
        setUserSelected(users[index])
        setEachEntry({ ...eachEntry, name: users[index].name });
        toggleUpdate()
    }

    const handleDeleteUser = index => {
        setUserSelected(users[index])
        toggleDelete()
    }

    const handleInputChange = e => {
        setEachEntry({ ...eachEntry, [e.target.name]: e.target.value });
    };

    const handleInputChangeUser = e => {
        setEachEntryUser({ ...eachEntryUser, [e.target.name]: e.target.value });
    };

    const handleInputChangeTask = e => {
        setEachEntryTask({
            ...eachEntryTask, [e.target.name]: e.target.name === 'task_state' ? e.target.checked : e.target.value
        });
    };

    const handleSubmit = e => {
        e.preventDefault();
        UserService.updateUser(userSelected.id, eachEntry)
            .then(_ => {
                retrieveUsers();
                toggleUpdate();
            })
            .catch(e => {
                console.log(e);
            });
    };

    const handleCreate = e => {
        e.preventDefault();
        UserService.createUser(eachEntryUser)
            .then(_ => {
                retrieveUsers();
                toggleCreate();
                setEachEntryUser({ name: '' });
            })
            .catch(e => {
                console.log(e);
            });
    };

    const handleConfirmDelete = e => {
        e.preventDefault();
        UserService.deleteUser(userSelected.id)
            .then(_ => {
                retrieveUsers();
                toggleDelete();
            })
            .catch(e => {
                console.log(e);
            });
    };

    const handleUpdateTask = task => {
        setEditingMode(true);
        setTaskSelected(task);
        if (task) {
            setEachEntryTask({
                ...eachEntryTask,
                description: task.description,
                task_state: task.task_state === TASK_STATUS.DONE ? true : false
            });
        }

    }

    const handleSaveTask = e => {
        e.preventDefault();
        if (taskSelected) {
            UserService.updateTask(
                taskSelected.id,
                {
                    ...eachEntryTask,
                    user: userSelected.id,
                    task_state: eachEntryTask.task_state ? TASK_STATUS.DONE : TASK_STATUS.TODO
                }
            )
                .then(_ => {
                    retrieveUsers();
                    toggleTask();
                    setEditingMode(false);
                    setEachEntryTask({ description: '' });
                })
                .catch(e => {
                    console.log(e);
                });
        }
        else {
            UserService.createTask(
                {
                    ...eachEntryTask,
                    task_state: eachEntryTask.task_state ? TASK_STATUS.DONE : TASK_STATUS.TODO,
                    user: userSelected.id
                }
            )
                .then(_ => {
                    retrieveUsers();
                    toggleTask();
                    setEditingMode(false);
                    setEachEntryTask({ description: '' });
                })
                .catch(e => {
                    console.log(e);
                });
        }
    }

    const handleCancelTask = e => {
        e.preventDefault();
        setEditingMode(false);
        setTaskSelected(null);
        setEachEntryTask(initialInputStateTask);
    }

    return (
        <div className="mt-4">
            <Row><Col sm="12" md={{ size: 9, offset: 1 }}><h2><b>Users List</b></h2></Col></Row>
            <Row>
                <Col sm="12" md={{ size: 9, offset: 1 }}>
                    <Table hover>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <MainList
                                users={users}
                                showModal={handleModal}
                                showModalUser={handleUpdateUser}
                                showModalDelete={handleDeleteUser} />
                        </tbody>
                    </Table>
                </Col>
            </Row>
            <Row><Col sm="12" md={{ size: 9, offset: 1 }}>
                <Button color="primary" onClick={() => toggleCreate()}>Add new user</Button>
            </Col></Row>
            <Modal isOpen={modalTask} toggle={toggleTask}>
                <ModalHeader toggle={toggleTask}>
                    {editingMode ? taskSelected ? 'UPDATE A TASK' : 'CREATE A TASK' : 'USERS TASKS'}
                </ModalHeader>
                <ModalBody>
                    {editingMode
                        ? (
                            <Form>
                                <FormGroup>
                                    <Label for="name">Task Description</Label>
                                    <Input
                                        type="text"
                                        onChange={handleInputChangeTask}
                                        name="description"
                                        placeholder="Enter task description"
                                        value={eachEntryTask.description} />
                                </FormGroup>
                                <FormGroup className='form-task' check>
                                    <Label check>
                                        <Input
                                            type="checkbox"
                                            name="task_state"
                                            onChange={handleInputChangeTask}
                                            value={eachEntryTask.task_state}
                                            checked={eachEntryTask.task_state}
                                        />
                                        <b>Mark as Done</b>
                                    </Label>
                                </FormGroup>
                                <Button onClick={handleSaveTask}>Save</Button>
                                <Button onClick={handleCancelTask}>Cancel</Button>
                            </Form>
                        ) : (
                            <div>
                                <table className="tasktable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Description</th>
                                            <th>State</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {userSelected ? userSelected.tasks.map(task => (
                                            <tr>
                                                <th>{task.id}</th>
                                                <td>{task.description}</td>
                                                <td>{TASK_STATUS[task.task_state]}</td>
                                                <td><Button color="info" onClick={() => handleUpdateTask(task)}>Update</Button></td>
                                            </tr>
                                        )) : ''}
                                    </tbody>
                                </table>
                                <Button color="primary" onClick={() => handleUpdateTask()}>Add new task</Button>
                            </div>
                        )}

                </ModalBody>
            </Modal>
            <Modal isOpen={modalUpdate} toggle={toggleUpdate}>
                <ModalHeader toggle={toggleUpdate}>UPDATE USER</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <Label for="name">User Name</Label>
                            <Input
                                type="text"
                                onChange={handleInputChange}
                                name="name"
                                placeholder="Enter user name"
                                value={eachEntry.name} />
                        </FormGroup>
                        <Button onClick={handleSubmit}>Submit</Button>
                    </Form>
                </ModalBody>
            </Modal>
            <Modal isOpen={modalDelete} toggle={toggleDelete}>
                <ModalHeader toggle={toggleDelete}>DELETE USER</ModalHeader>
                <ModalBody>
                    {userSelected && (
                        <div>
                            <div className='delete-cont'>Are you sure you want to delete {userSelected.name}?</div>
                            <div>
                                <Button onClick={handleConfirmDelete}>Yes</Button>
                                <Button onClick={() => toggleDelete()}>Cancel</Button>
                            </div>
                        </div>
                    )}
                </ModalBody>
            </Modal>
            <Modal isOpen={modalCreate} toggle={toggleCreate}>
                <ModalHeader toggle={toggleCreate}>CREATE USER</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <Label for="name">User Name</Label>
                            <Input
                                type="text"
                                onChange={handleInputChangeUser}
                                name="name"
                                placeholder="Enter user name"
                                value={eachEntryUser.name} />
                        </FormGroup>
                        <Button onClick={handleCreate}>Submit</Button>
                    </Form>
                </ModalBody>
            </Modal>
        </div>
    );
};
