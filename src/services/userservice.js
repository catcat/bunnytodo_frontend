import http from "../http-common";

const getAllUsers = () => {
    return http.get("/users/full_user/");
};

const updateUser = (id, data) => {
    return http.put(`/users/${id}/`, data);
};

const createUser = data => {
    return http.post("/users/", data);
};

const deleteUser = id => {
    return http.delete(`/users/${id}/`);
};

const getAllTasks = () => {
    return http.get("/tasks/");
};

const createTask = data => {
    return http.post("/tasks/", data);
};

const updateTask = (id, data) => {
    return http.put(`/tasks/${id}/`, data);
};

const getAllTasksByUser = (id) => {
    return http.get(`/users/${id}/all_todo_byuser/`);
};


export default {
    getAllUsers,
    updateUser,
    deleteUser,
    getAllTasks,
    createTask,
    updateTask,
    getAllTasksByUser,
    createUser,
};